# 12_1

buffet = ('carne', 'peixe', 'sorvete', 'salada', 'chocolate')

def menu(buffet):
	for food in buffet:
		print (food)

menu(buffet)

print ('')
# garantindo que o python rejeite a alteração
# na tupla

# buffet[0] = 'porco'

buffet = ('cebola', 'tomate', 'sorvete', 'salada', 'chocolate')

menu(buffet)