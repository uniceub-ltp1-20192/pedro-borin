# P5_4

age = int(input("Sua idade: "))
semesters = int(input("Qual semestre?: "))

#assumindo que o curso tenha 10 semestres

total_semesters = 10

gap_semesters = total_semesters - semesters
semesters_in_year = gap_semesters / 2
conclusion = age + semesters_in_year

print ("\nVocê terminará o curso em " + str(semesters_in_year) + " anos.")
print ("Você terá " + str(conclusion) + " quando terminar o curso.")
