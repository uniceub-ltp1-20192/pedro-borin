# 11_1

# Pegando a lista do exercicio 8_3

continents = ['Europa', 'Asia', 'Africa', 'Oceania', 'Antartida', 'America']

print ('Os primeiros 3 itens da lista são:', continents[:3])
print ('Os últimos 3 itens da lista são:', continents[-3:])
print ('Os 3 itens no meio da lista são:', continents[1:4])