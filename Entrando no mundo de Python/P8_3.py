# 8_3

continents = ['Europa', 'Asia', 'Africa', 'Oceania', 'Antartida', 'America']

print ('Lista: ' + str(continents))
print ('Em ordem crescente: ' + str(sorted(continents)))
print ('Em ordem decrescente: ' + str(sorted(continents, reverse=True)))

continents.reverse()

print ('Lista de tras pra frente: ' + str((continents)))

print ('Numero de continentes: ' + str(len(continents)))