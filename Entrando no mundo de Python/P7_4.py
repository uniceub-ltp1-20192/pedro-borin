# 7_4

# Lista com João, Pedro e Maria
dinner_list = ['João', 'Pedro', 'Maria']
vacilao = dinner_list[0]

# Luana, Pedro e Maria
dinner_list[0] = 'Luana'

print ("Convido você, " + dinner_list[0] + ", para jantar.")
print ("Quero que você, " + dinner_list[1] + ", compareça no jantar.")
print ("Venha, " + dinner_list[2] + ", no jantar de domingo.")

print ("\nSinto informar que o " + vacilao + " não estará presente no jantar.")

print ("\nFala " + dinner_list[0] + ", " + dinner_list[1] + " e " + dinner_list[2] + "!"
	"\nConsegui mais 3 lugares!")

# Lista atual: Daniel, Luana, Laura, Pedro, Maria e Lineu
dinner_list.insert(0, "Daniel")
dinner_list.insert(2, "Laura")
dinner_list.append("Lineu")

print ("\nVem pro jantar também " + dinner_list[0] + "!")
print ("Junte-se a nós " + dinner_list[2] + "!")
print ("Você foi convidado para jantar, " + dinner_list[-1] + ".")

print ("\nApenas dois dos convidados permanecerão.")

# Deixando apenas Daniel e Luana na lista
# guests 1 à 4 respectivamente (Laura, Pedro, Maria, Lineu)
guest_1 = dinner_list.pop(2)
guest_2 = dinner_list.pop(2)
guest_3 = dinner_list.pop(2)
guest_4 = dinner_list.pop(2)

print ("\nVocê está fora " + guest_1 + "!")
print ("Perdeu sua vaga " + guest_2 + "!")
print ("Não há mais lugares " + guest_3 + "!")
print ("Você não poderá mais vir " + guest_4 + "!")

print ("\n" + dinner_list[0] + " e " + dinner_list[1] + ", vocês ainda estão convidados.")

del dinner_list[0]
del dinner_list[0]

print ("\n" + str(dinner_list))