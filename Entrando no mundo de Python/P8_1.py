# 8_1

places = ['Europa', 'Asia', 'Africa', 'Oceania', 'Antartida']

# Lista original
print (places)

# Lista em ordem alfabética
print (sorted(places))

# Lista original não modificada
print (places)

# Lista em ordem alfabética reversa
print (sorted(places, reverse=True))

# Lista original não modificada
print (places)

# Reverte a lista e printa
places.reverse()
print (places)

# Volta à lista original
places.reverse()
print (places)

# Organiza em ordem alfabética
places.sort()
print (places)

# Organiza em ordem alfabética reversa
places.sort(reverse=True)
print (places)