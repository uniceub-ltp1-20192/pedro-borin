#Apenas pessoas de 2 sobrenomes, mais de 1,60 e que estejam acompanhadas entram.
#Se tiver 1,50 e trouxer 4 acompanhantes, for a 100 pessoa a entrar na cabine 10 ganha premio

cabine = 1
people = 0

print('Cadastro para roda gigante')

#processo de validação dos dados inseridos
#se inválido, eliminar

import re

#Validando nome
name = input('Insira seu nome completo: ')

def validate_name(x):
	p = re.compile('[^a-zA-Z\s]')

	namev = p.search(name)

	if namev != None:
		print ("Nao pode entrar!")
		exit()

vname = validate_name(name)

#Validando altura
height = input('Insira sua altura: ')

def validate_height(x):
	try:
		val = float(height)
	except ValueError:
		print("Nao pode entrar!")
		exit()

vheight = validate_height(height)

#Validando acompanhantes
friends = input('Quantos acompanhantes?: ')

def validate_friends(x):
	try:
		val = int(friends)
	except ValueError:
		print("Nao pode entrar!")
		exit()

vfriends = validate_friends(friends)

#conferir os dados inseridos
#se inelegíveis, eliminar

#Checando nome
def check_name(x):
	res = len(name.split())
	if res != 3:
		if (height == 1.50 and friends == 4 and cabine == 10 and people == 99):
			print ("Parabens! Voce ganhou o premio do palhaco louco!")
			exit()
		print ("Nao pode entrar!")
		exit()

cname = check_name(name)

#Checando altura
def check_height(x):
    VH = float(height)
	if not (VH >= 1.60 and VH <= 2.50):
		print ("Nao pode entrar!")
		exit()

cheight = check_height(height)

#Checando acompanhantes
def check_friends(x):
	if not int(friends) >= 1:
		print ("Nao pode entrar!")
		exit()

cfriends = check_friends(friends)

print ("Voce esta elegivel para entrar na roda gigante!")

#Contagem de cabine e pessoas
cabine = cabine + 1

#Resetando cabine para 1 se cabine for 11 ou maior
def reset_cabine(x):
	if cabine > 10:
		cabine = 1
		people = people + 1

rcabine = reset_cabine(cabine)