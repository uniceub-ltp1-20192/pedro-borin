# para a quantidade de notas solicita a nota e peso
def start_loop():

	# variaveis para armazenar a soma das notas
	# a soma das notas com peso e a soma dos pesos 
	sum_grades          = 0
	sum_weighted_grades = 0
	sum_weighted        = 0

	# quantidade inicial de notas
	num_grades = 1

	# variavel para entrar no loop
	answer == "s"

	while (answer == "s"):
		
		grade  = float(input("Nota: "))
		weight = int(input("Peso (1-5): "))

		# adiciona a soma das notas
		sum_grades = sum_grades + grade

		# adiciona a soma das notas com peso
		sum_weighted_grades = sum_weighted_grades + (grade * weight)

		# adiciona a soma de pesos
		sum_weighted = sum_weighted + weight
		answer = input("Acrescentar nota? (s/n): ")
		if answer == "s":
			num_grades = num_grades + 1
	
	return sum_grades, num_grades, sum_weighted_grades, sum_weighted

# chama a função loop
sloop = start_loop()

# apresenta o resultado final
def show_results(x):
	print("\n")
	print("-----------------------------------------------")
	print("Média: ", sum_grades/num_grades)
	print("Média ponderada: ", sum_weighted_grades/sum_weighted)

# chama a função results
sreults = show_results(sloop)