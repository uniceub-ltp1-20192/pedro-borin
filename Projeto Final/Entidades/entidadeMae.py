"""
Grupo de LTP1:

Filipi Carlos Rodrigues
RA: 21901312

Lauro Kennedy
RA: 21907445

Pedro Henrique Batista
RA: 21910157
"""
class Pessoa:
  
  def __init__(self,identificador = 0,nome="",sexo="",idade=""):
    self._nome = nome
    self._sexo = sexo
    self._idade = idade

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def nome(self):
    return self._nome

  @nome.setter
  def nome(self,nome):
    self._nome = nome

  @property
  def sexo(self):
    return self._sexo

  @sexo.setter
  def sexo(self,sexo):
    self._sexo = sexo

  @property
  def idade(self):
    return self._idade

  @idade.setter
  def idade(self,idade):
    self._idade = idade

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  def concordar(self):
    print("Sim")

  def negar(self):
    print("Nao")